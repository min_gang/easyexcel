package com.easyexcel.writemodeloflaabletest;

import com.easyexcel.annotation.Cell;

public class Students {
	private String name;
	@Cell(name = "age")
    private int nl;

    public int getNl() {
        return nl;
    }

    public void setNl(int nl) {
        this.nl = nl;
    }

    public Students(String name, int age){
        this.name = name;
        this.nl = age;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
