package com.easyexcel.writemodeloflaabletest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.easyexcel.write.WriteExcel;
import com.easyexcel.write.WriteModelExcel;

public class WriteModelOfLableTest {

	@Before
	public void setUp() throws Exception {
	}

    @Test
	public void writeModelOfLableTest() throws IllegalArgumentException, IllegalAccessException, IOException {
		WriteExcel<Students> we = new WriteModelExcel<>();
		Map<String,Object> param = new HashMap<>();
		param.put("inFilePath", "d:\\model.xlsx");//读取文件的目录必须有，可以传也可以用注解配,如果传以传为主
		param.put("outFilePath", "d:\\outmodel.xlsx");//生成路径必须有，可以传也可以用注解配,如果传以传为主
		List<Students> list = new ArrayList<Students>();
		list.add(new Students("张三111111111111111111111111111111111111111",25));
		we.write(param, list);
	}

}
