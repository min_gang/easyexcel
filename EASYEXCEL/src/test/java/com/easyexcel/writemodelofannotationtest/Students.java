package com.easyexcel.writemodelofannotationtest;

import com.easyexcel.annotation.Cell;
import com.easyexcel.annotation.Excel;
import com.easyexcel.annotation.Ingroe;
import com.easyexcel.annotation.Sheet;
@Excel(inFilePath="d:\\modelann.xlsx",outFilePath="d:\\outmodelann.xlsx")
@Sheet(sheetNum=1)
public class Students {
	@Cell(rowNum=1,columnNum="b")
	private String name;
	@Cell(rowNum=3,columnNum="b")
	private int age;
	@Ingroe
	private double grade;
	public Students(String name,int age){
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getGrade() {
		return grade;
	}
	public void setGrade(double grade) {
		this.grade = grade;
	}
}
