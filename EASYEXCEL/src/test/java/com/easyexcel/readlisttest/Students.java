package com.easyexcel.readlisttest;

import com.easyexcel.annotation.Cell;
import com.easyexcel.annotation.Excel;
@Excel(beginRow=2,inFilePath="d:\\students.xlsx")
public class Students {
	@Cell(columnNum="2")
	private String name;
	@Cell(columnNum="c")//or@Cell(columnNum="C")
	private int age;
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "name="+name+":age="+age;
	}
}
