package com.easyexcel.writelistTest;

import com.easyexcel.annotation.Cell;
import com.easyexcel.annotation.Excel;
import com.easyexcel.annotation.Sheet;
@Excel(beginRow=1,dataHeader="姓名:1,年龄:3,成绩qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq:4",outFilePath="d:\\test.xlsx")
@Sheet(/*sheetSize=2000,*/sheetName="测试")
public class Students {
	@Cell(columnNum="1")
	private String name;
	@Cell(columnNum="c")
	private int age;
	@Cell(columnNum="4")
	private double grade;
	public Students(String name,int age,double grade){
		this.name = name;
		this.age = age;
		this.grade = grade;
	}
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "name="+name+":age="+age;
	}
}
